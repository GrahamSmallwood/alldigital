//
//  ResultsCell.h
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultsCell : UITableViewCell

-(void)LoadData:(NSDictionary *)tData;

@end
