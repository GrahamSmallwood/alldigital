//
//  DetailsViewController.m
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import "DetailsViewController.h"
#import "AppViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <AVFoundation/AVFoundation.h>

@interface DetailsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *mPicture;
@property (weak, nonatomic) IBOutlet UILabel *mTitle;
@property (weak, nonatomic) IBOutlet UILabel *mRating;

@property (weak, nonatomic) IBOutlet UILabel *mReleaseDate;
@property (weak, nonatomic) IBOutlet UILabel *mPrice;
@property (weak, nonatomic) IBOutlet UILabel *mPurchaseURL;
@property (weak, nonatomic) IBOutlet UILabel *mPreviewURL;
@property (weak, nonatomic) IBOutlet UITextView *mDescription;
- (IBAction)Back:(id)sender;
- (IBAction)Purchase:(id)sender;
- (IBAction)Preview:(id)sender;

@property (nonatomic, strong)AVPlayer *mPreviewPlayer;

@end

@implementation DetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
	NSDictionary *tData = self.mVCData;
	
	self.mTitle.text = [tData objectForKey:@"trackName"];
	self.mRating.text = [tData objectForKey:@"contentAdvisoryRating"];
	[self.mPicture setImageWithURL:[tData objectForKey:@"artworkUrl60"]];
	
	self.mReleaseDate.text = [self userVisibleDateTimeStringForRFC3339DateTimeString:[tData objectForKey:@"releaseDate"]];
	self.mPrice.text = [self userVisibleCurrencyStringFromAmount:[tData objectForKey:@"collectionPrice"] andCurrency:[tData objectForKey:@"currency"]];
	self.mPurchaseURL.text = [tData objectForKey:@"collectionViewUrl"];
	self.mPreviewURL.text = [tData objectForKey:@"previewUrl"];
	self.mDescription.text = [tData objectForKey:@"longDescription"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Back:(id)sender
{
	if( self.mPreviewPlayer )
		[self.mPreviewPlayer pause];
	self.mPreviewPlayer = nil;
	[[AppViewController The] SwitchTo:VC_Results];
}

- (IBAction)Purchase:(id)sender // Doesn't work in simulator
{
	if( self.mPreviewPlayer )
		[self.mPreviewPlayer pause];
	self.mPreviewPlayer = nil;
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString: self.mPurchaseURL.text]];
}

- (IBAction)Preview:(id)sender// AVAudioPlayer doesn't do streaming, and this doesn't work on simulator
{
	self.mPreviewPlayer = [AVPlayer playerWithURL:[NSURL URLWithString:self.mPreviewURL.text]];
	[self.mPreviewPlayer play];
}

- (NSString *)userVisibleDateTimeStringForRFC3339DateTimeString:(NSString *)rfc3339DateTimeString {
	// From Apple NSDate definition

    /*
	 Returns a user-visible date time string that corresponds to the specified
	 RFC 3339 date time string. Note that this does not handle all possible
	 RFC 3339 date time strings, just one of the most common styles.
     */
	
    NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
	
    [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
    // Convert the RFC 3339 date time string to an NSDate.
    NSDate *date = [rfc3339DateFormatter dateFromString:rfc3339DateTimeString];
	
    NSString *userVisibleDateTimeString;
    if (date != nil) {
        // Convert the date object to a user-visible date string.
        NSDateFormatter *userVisibleDateFormatter = [[NSDateFormatter alloc] init];
        assert(userVisibleDateFormatter != nil);
		
        [userVisibleDateFormatter setDateFormat:@"MM/dd/yyyy"];
		
        userVisibleDateTimeString = [userVisibleDateFormatter stringFromDate:date];
    }
    return userVisibleDateTimeString;
}

-(NSString *)userVisibleCurrencyStringFromAmount:(NSString*)tAmount andCurrency:(NSString*)tCurrency
{
	NSNumberFormatter *tFormat = [[NSNumberFormatter alloc] init];
	[tFormat setNumberStyle:NSNumberFormatterCurrencyStyle];
	[tFormat setCurrencyCode:tCurrency];
	return [tFormat stringFromNumber:[NSNumber numberWithFloat:[tAmount floatValue]]];
}

@end
