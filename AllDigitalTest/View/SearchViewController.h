//
//  SearchViewController.h
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import "BaseViewController.h"

@interface SearchViewController : BaseViewController <UITextFieldDelegate>

@end
