//
//  BaseViewController.m
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import "BaseViewController.h"
#import "AppViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

@synthesize mVCData;

-(void)TransitionOff
{
	[UIView beginAnimations:@"VCSlide" context:nil];
	{
		self.view.frame = CGRectMake(-self.view.frame.size.width, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
		[UIView setAnimationDelegate:[AppViewController The]];
		[UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
	}
	[UIView commitAnimations];
}

-(void)TransitionOn
{
	self.view.frame = CGRectMake(self.view.frame.size.width, -self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
	[UIView beginAnimations:@"VCSlide" context:nil];
	{
		self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
	}
	[UIView commitAnimations];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
