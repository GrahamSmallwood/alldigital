//
//  ResultsViewController.m
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import "ResultsViewController.h"
#import "AppViewController.h"
#import "ResultsCell.h"
#import "AppModel.h"

@interface ResultsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *mResultsTable;
@property (strong, nonatomic) IBOutlet ResultsCell *mCellFactory;
- (IBAction)Back:(id)sender;

@end

@implementation ResultsViewController

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	ResultsCell *tCell = [tableView dequeueReusableCellWithIdentifier:@"ResultsCell"];
	if( tCell == nil )
	{
		[[NSBundle mainBundle] loadNibNamed:@"ResultsCell" owner:self options:nil];
		tCell = self.mCellFactory;
		self.mCellFactory = nil;
	}
	
	[tCell LoadData:[[AppModel The] GetDataForResult:indexPath.row]];
	
	return tCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[[AppViewController The] SwitchTo:VC_Details withData:[[AppModel The] GetDataForResult:indexPath.row]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 64;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [[AppModel The] GetNumberSearchResults];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Back:(id)sender
{
	[[AppViewController The] SwitchTo:VC_Search];
}
@end
