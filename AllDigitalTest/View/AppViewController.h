//
//  AppViewController.h
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum VCType
{
	VC_Splash,
	VC_Search,
	VC_Results,
	VC_Details
}
VCType;

@interface AppViewController : UIViewController

+(AppViewController *)The;
-(void)SwitchTo:(VCType)tScreen;
-(void)SwitchTo:(VCType)tScreen withData:(NSDictionary*)tData;
- (void)animationFinished:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context;


@end
