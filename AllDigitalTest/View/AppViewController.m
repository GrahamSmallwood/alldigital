//
//  AppViewController.m
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import "AppViewController.h"
#import "BaseViewController.h"
#import "DetailsViewController.h"
#import "ResultsViewController.h"
#import "SearchViewController.h"
#import "SplashViewController.h"

@interface AppViewController ()
@property (nonatomic, strong) BaseViewController *mCurrentVC;
@property (nonatomic) VCType mPendingVCType;
@property (nonatomic, strong) NSDictionary *mPendingData;
@end

static AppViewController *sThe;

@implementation AppViewController

+(AppViewController *)The
{
	return sThe;
}

-(void)SwitchTo:(VCType)tScreen
{
	self.mPendingVCType = tScreen;
	self.mPendingData = nil;
	[self.mCurrentVC TransitionOff];
}

-(void)SwitchTo:(VCType)tScreen withData:(NSDictionary*)tData
{// This is a dupe instead of calling the vanilla one because there is no good order to save tData in
	self.mPendingVCType = tScreen;
	self.mPendingData = tData;
	[self.mCurrentVC TransitionOff];
}

- (void)animationFinished:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
	[self.mCurrentVC.view removeFromSuperview];
	self.mCurrentVC = nil;
	
	if( self.mPendingVCType == VC_Search )
		self.mCurrentVC = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
	else if( self.mPendingVCType == VC_Results )
		self.mCurrentVC = [[ResultsViewController alloc] initWithNibName:@"ResultsViewController" bundle:nil];
	else if( self.mPendingVCType == VC_Details )
		self.mCurrentVC = [[DetailsViewController alloc] initWithNibName:@"DetailsViewController" bundle:nil];
	
	self.mCurrentVC.mVCData = [self.mPendingData mutableCopy];
	
	[self.view addSubview:self.mCurrentVC.view];
	[self.mCurrentVC TransitionOn];
}

- (id)init
{
    self = [super init];
    if (self)
	{
        // Custom initialization
		sThe = self;

		self.mCurrentVC = [[SplashViewController alloc] initWithNibName:@"SplashViewController" bundle:nil];
		
		[self.view addSubview:self.mCurrentVC.view];
		[self.mCurrentVC TransitionOn];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
