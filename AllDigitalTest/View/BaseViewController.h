//
//  BaseViewController.h
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

-(void)TransitionOff;
-(void)TransitionOn;

@property (nonatomic, strong) NSMutableDictionary *mVCData;
@end
