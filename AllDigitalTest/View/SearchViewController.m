//
//  SearchViewController.m
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import "SearchViewController.h"
#import "AppViewController.h"
#import "AppModel.h"

@interface SearchViewController ()
@property (weak, nonatomic) IBOutlet UITextField *mSearchField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mSpinner;
- (IBAction)Search:(id)sender;

@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self.mSearchField resignFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(OnSearchSuccess:) name:@"SearchSuccess" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(OnSearchFailed:) name:@"SearchFailed" object:nil];
}

-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"SearchSuccess" object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"SearchFailed" object:nil];
}

-(void)OnSearchSuccess:(NSNotification*)tNotification
{
	[self.mSpinner stopAnimating];
	[[AppViewController The] SwitchTo:VC_Results];
}

-(void)OnSearchFailed:(NSNotification*)tNotification
{
	[self.mSpinner stopAnimating];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self Search:nil];
	return true;
}

- (IBAction)Search:(id)sender
{
	[self.mSpinner startAnimating];
	[[AppModel The] PerformSearch:self.mSearchField.text];
}
@end
