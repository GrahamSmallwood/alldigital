//
//  ResultsCell.m
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import "ResultsCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ResultsCell ()
@property (weak, nonatomic) IBOutlet UIImageView *mPicture;
@property (weak, nonatomic) IBOutlet UILabel *mTitle;
@property (weak, nonatomic) IBOutlet UILabel *mRating;

@end

@implementation ResultsCell

-(void)LoadData:(NSDictionary *)tData
{
	self.mTitle.text = [tData objectForKey:@"trackName"];
	self.mRating.text = [tData objectForKey:@"contentAdvisoryRating"];
	[self.mPicture setImageWithURL:[tData objectForKey:@"artworkUrl60"]];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
