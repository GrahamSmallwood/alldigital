//
//  SplashViewController.m
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import "SplashViewController.h"
#import "AppViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

-(void)SplashTimeout:(NSTimer*)tTimer
{
	[[AppViewController The] SwitchTo:VC_Search];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		[NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(SplashTimeout:) userInfo:nil repeats:false];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
