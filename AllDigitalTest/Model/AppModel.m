//
//  AppModel.m
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import "AppModel.h"
#import "WebLoader.h"
#import "NSNotificationCenter+MainThread.h"

static AppModel *sThe;

@interface AppModel ()
@property (nonatomic, strong) NSArray *mSearchResults;
@end

@implementation AppModel

+(AppModel *)The
{
	return sThe;
}

-(void)SearchResultsReceived:(NSDictionary *)tResults
{
	self.mSearchResults = [tResults objectForKey:@"results"];
	[[NSNotificationCenter defaultCenter] postNotificationName:@"SearchSuccess" object:nil];
}

- (id)init
{
    self = [super init];
    if (self) {
        // Initialize self.
		sThe = self;
    }
    return self;
}

-(void)PerformSearch:(NSString*)tSearchTerm
{
	[WebLoader StartWebCall:kSearchRequest data:@{@"Search" : tSearchTerm}];
}

-(int)GetNumberSearchResults
{
	return self.mSearchResults.count;
}

-(NSDictionary *)GetDataForResult:(int)tWhich
{
	return [self.mSearchResults objectAtIndex:tWhich];
}

@end
