//
//  AppModel.h
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppModel : NSObject
+(AppModel *)The;

-(void)SearchResultsReceived:(NSDictionary *)tResults;

-(void)PerformSearch:(NSString*)tSearchTerm;
-(int)GetNumberSearchResults;
-(NSDictionary *)GetDataForResult:(int)tWhich;

@end
