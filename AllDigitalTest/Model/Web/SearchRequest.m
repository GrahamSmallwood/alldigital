//
//  SearchRequest.m
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import "SearchRequest.h"
#import "NSNotificationCenter+MainThread.h"
#import "AppModel.h"

@implementation SearchRequest

-(NSString *)CreateAddressFromData:(NSMutableDictionary*)tData
{
    return [NSString stringWithFormat:@"%@search?term=%@",
			[self GetWebPrefix],
			[tData objectForKey:@"Search"]
			];
}

-(void)FullDataDownloaded:(NSString*)tFileName
{
	NSData* tFileData = [NSData dataWithContentsOfFile:tFileName];
	NSDictionary *tRaw = [NSJSONSerialization JSONObjectWithData:tFileData options:NSJSONReadingMutableContainers error:nil];
	[[AppModel The] SearchResultsReceived:tRaw];
}
-(NSString *)RequestString
{
	return @"Search";
}
-(void)DataDownloadFailed
{
	[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:@"SearchFailure" object:nil userInfo:nil];
}

@end

/*
 Printing description of tRaw:
 {
 resultCount = 50;
 results =     (
 {
 artistId = 62820413;
 artistName = "Arctic Monkeys";
 artistViewUrl = "https://itunes.apple.com/us/artist/arctic-monkeys/id62820413?uo=4";
 artworkUrl100 = "http://a4.mzstatic.com/us/r30/Features6/v4/77/e5/d5/77e5d538-24df-e160-5830-5964bf6556c1/dj.oafezkuq.100x100-75.jpg";
 artworkUrl30 = "http://a2.mzstatic.com/us/r30/Features6/v4/77/e5/d5/77e5d538-24df-e160-5830-5964bf6556c1/dj.oafezkuq.30x30-50.jpg";
 artworkUrl60 = "http://a4.mzstatic.com/us/r30/Features6/v4/77/e5/d5/77e5d538-24df-e160-5830-5964bf6556c1/dj.oafezkuq.60x60-50.jpg";
 collectionCensoredName = AM;
 collectionExplicitness = notExplicit;
 collectionId = 663097964;
 collectionName = AM;
 collectionPrice = "9.99";
 collectionViewUrl = "https://itunes.apple.com/us/album/do-i-wanna-know/id663097964?i=663097965&uo=4";
 country = USA;
 currency = USD;
 discCount = 1;
 discNumber = 1;
 kind = song;
 previewUrl = "http://a64.phobos.apple.com/us/r1000/025/Music6/v4/fa/a6/51/faa6513d-03b5-9b84-0086-31acd6ab20c4/mzaf_4309887093295976361.plus.aac.p.m4a";
 primaryGenreName = Alternative;
 radioStationUrl = "https://itunes.apple.com/us/station/idra.663097965";
 releaseDate = "2013-09-09T07:00:00Z";
 trackCensoredName = "Do I Wanna Know?";
 trackCount = 12;
 trackExplicitness = notExplicit;
 trackId = 663097965;
 trackName = "Do I Wanna Know?";
 trackNumber = 1;
 trackPrice = "1.29";
 trackTimeMillis = 271716;
 trackViewUrl = "https://itunes.apple.com/us/album/do-i-wanna-know/id663097964?i=663097965&uo=4";
 wrapperType = track;
 },
 .
 .
 .
*/