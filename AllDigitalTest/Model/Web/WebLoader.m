//
//  WebLoader.m
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

#import "WebLoader.h"
#import "AppDelegate.h"
#import "SearchRequest.h"

static NSMutableArray *sInProgress;

@interface WebLoader (Private)
-(void)FinishConnection;
-(void)TimeoutAbort:(NSTimer*)tTimer;
-(NSString*)CreateAddressFromData:(NSDictionary*)tData;
@end




@implementation WebLoader

@synthesize m_Connection;
@synthesize m_StreamedData;
@synthesize m_IsTemp;

+(void)StartWebCall:(WebCallType)tType data:(NSDictionary*)tData
{
    WebLoader *tNew = nil;
    if( tType == kSearchRequest )
        tNew = [[SearchRequest alloc] init];
	
    if( ![tNew InternalWebCall:tData] )
		return;
    
	tNew.m_Data = [tData copy];
    [sInProgress addObject:tNew];
}

-(NSString*)GetWebPrefix
{
	return @"https://itunes.apple.com/";
}

-(NSString *)CreateAddressFromData:(NSDictionary*)tData // The data is for Gets that want a bunch of params
{
	return [self GetWebPrefix];
}

-(NSData*)GetBody // The body is for Posts that want a bunch of params
{
	return nil;
}

-(bool)WantsHeaders
{
	return true;
}

-(bool)QuantityCheckOkay
{
	// Some types of calls throttle themselves.
	return true;
}

-(bool)InternalWebCall:(NSDictionary *)tParameters
{
    if( !tParameters )
        return false;
	if( ![self QuantityCheckOkay] )
		return false;
	
	m_Parameters = [tParameters copy];
	m_Address = [self CreateAddressFromData:m_Parameters];
	m_BodyData = [self GetBody];
	
	
	if( m_Connection != nil )
		[self FinishConnection];// Wrap up the old one if we are overwriting it
	
	m_Address = [m_Address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	// Build Url
	NSURL *tUrl = [NSURL URLWithString:m_Address];
	if( tUrl == nil )
		return false;
	
	// Create url request
	NSMutableURLRequest *tRequest = [NSMutableURLRequest requestWithURL:tUrl];
	if( tRequest == nil )
		return false;
	
	// Optional body for some types
	if( m_BodyData != nil )
	{
		[tRequest setHTTPMethod:@"POST"];
		[tRequest setValue:[NSString stringWithFormat:@"%d", [m_BodyData length]] forHTTPHeaderField:@"Content-Length"];
		[tRequest setHTTPBody:m_BodyData];
	}
	
	// Authenticate Bing-style if needed
	[self AddAuthentication:tRequest];
	
	// Start connection
	m_Connection = [NSURLConnection connectionWithRequest:tRequest delegate:self];
	if( m_Connection == nil )
		return false;
	[self OnConnectionStart];
	
	// Bookkeeping
	m_DownloadSuccess = false;
	m_StreamedData = [[NSMutableData alloc]init];
	
	int tTimeout = 10;
	m_TimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:tTimeout
													  target:self
													selector:@selector(TimeoutAbort:)
													userInfo:nil
													 repeats:false];
	
	return true;
}

-(void)AddAuthentication:(NSMutableURLRequest*)tRequest
{
}

-(void)OnConnectionStart
{
}

-(void)TimeoutAbort:(NSTimer*)tTimer
{
	m_DownloadSuccess = false;
	[self FinishConnection];
}

-(void)FinishConnection
{
	// Close connection
    if( m_Connection != nil )
	{
        [m_Connection cancel];
        m_Connection = nil;
    }
	
	if( m_TimeoutTimer )
	{
		[m_TimeoutTimer invalidate];
		m_TimeoutTimer = nil;
	}
	
	if( m_DownloadSuccess && m_StreamedData != nil)
	{
		// Ask for temp file path
		CFUUIDRef uuid;
		uuid = CFUUIDCreate(NULL);
		CFStringRef uuidStr;
		uuidStr = CFUUIDCreateString(NULL, uuid);
		
		NSString *tPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@.txt", uuidStr]];
		
		
		[m_StreamedData writeToFile:tPath atomically:true];
		
        [self FullDataDownloaded:tPath];
		CFRelease(uuidStr);
		CFRelease(uuid);
		
	}
	else
	{
        [self DataDownloadFailed];// sad panda
	}
	
	// Bookkeeping
	m_StreamedData = nil;
    [sInProgress removeObject:self];
}

// This callback is for start of transfer
- (void)connection:(NSURLConnection *)theConnection didReceiveResponse:(NSURLResponse *)response
{
	// Check response for error, stop
	NSHTTPURLResponse * tHttpResponse = (NSHTTPURLResponse *) response;
	
	if( (tHttpResponse.statusCode / 100) != 2 )// Really?  No func for this?
		[self FinishConnection];
}

// This callback is to show lack of connection
- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error
{
	// Stop
	[self FinishConnection];
}

// This callback is for each packet of data we get back
- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)data
{
	[m_StreamedData appendData:data];
}

// This callback marks success
- (void)connectionDidFinishLoading:(NSURLConnection *)theConnection
{
	m_DownloadSuccess = true;
	[self FinishConnection];
}



-(void)FullDataDownloaded:(NSString*)tFileName
{
}
-(NSString *)RequestString
{
	return @"X";
}
-(void)DataDownloadFailed
{
}

@end