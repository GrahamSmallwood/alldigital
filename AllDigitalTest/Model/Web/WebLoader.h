//
//  WebLoader.h
//  AllDigitalTest
//
//  Created by MacBook Pro on 3/8/14.
//  Copyright (c) 2014 MacBook Pro. All rights reserved.
//

/*
 The underscores in the variable names in this file alone are because I have carrying it forward from my very first contract years ago.  
 I add to it every project.  It's good luck.
 */

#import <Foundation/Foundation.h>

typedef enum WebCallType
{
	kSearchRequest
} WebCallType;

@interface WebLoader : NSObject <NSURLConnectionDelegate>
{
    NSURLConnection *m_Connection;
    NSMutableData *m_StreamedData;
	
	bool m_DownloadSuccess;
	bool m_IsTemp;
	
	NSDictionary *m_Parameters;
	NSString *m_Address;
	NSData *m_BodyData;
	
	NSTimer *m_TimeoutTimer;
}

@property (retain, nonatomic) NSURLConnection *m_Connection;
@property (retain, nonatomic) NSMutableData *m_StreamedData;
@property (nonatomic) bool m_IsTemp;
@property (retain, nonatomic) NSDictionary *m_Data;

+(void)StartWebCall:(WebCallType)tType data:(NSDictionary*)tData;

-(NSString*)GetWebPrefix;

// Protected
-(void)FinishConnection;
-(void)AddAuthentication:(NSMutableURLRequest*)tRequest;

@end

